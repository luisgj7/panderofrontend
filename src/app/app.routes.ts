import {  RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { ClientComponent } from './components/client/client.component';
import { VentasComponent } from './components/ventas/ventas.component';

const ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'search/:term', component: SearchComponent },
  { path: 'client/:id', component: ClientComponent },
  { path: 'ventas', component: VentasComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

export const APP_ROUTING = RouterModule.forRoot(ROUTES, { useHash:true });

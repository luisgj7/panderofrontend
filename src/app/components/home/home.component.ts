import { Component, OnInit } from '@angular/core';
import { PanderoService } from '../../services/pandero.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {

  clients:any[] = [];
  loading: boolean;
  error:boolean;
  messageError:string;

  constructor(private pandero: PanderoService) {
    this.pandero.getAllClients()
        .subscribe( (data:any) => {
          console.log(data);
          this.clients = data;
          this.loading=false;
        },(errorService) => {
          console.log(errorService);
          this.error = true;
          this.loading = false;
        });
  }


}

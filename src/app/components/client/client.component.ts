import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PanderoService } from '../../services/pandero.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styles: []
})
export class ClientComponent {

  loading: boolean = true;
  client: any = [];

  constructor(private router:ActivatedRoute,
              private pandero:PanderoService) {
    this.router.params.subscribe( params => {
      this.getClientById(params['id']);
    });
  }

  getClientById(id:string){
    this.pandero.getClientById(id)
      .subscribe(data => {
        this.client = data;
        console.log(data);
        this.loading = false;
      });
  }

}

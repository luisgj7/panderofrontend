import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PanderoService } from '../../services/pandero.service';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styles: []
})
export class VentasComponent {

  vendedores:any[] = [];
  certificados:any[] = [];
  productos:any[] = [];
  asociados:any[] = [];

  Ok:boolean = false;

  constructor(private pandero: PanderoService) {
    this.pandero.getAllClients()
      .subscribe((data:any) => {
        this.asociados = data;
      });

    this.pandero.getAllCertificados()
      .subscribe((data:any) => {
        this.certificados = data;
      });

    this.pandero.getAllProductos()
    .subscribe((data:any) => {
      this.productos = data;
    });

    this.pandero.getAllVendedores()
        .subscribe( (data:any) => {
          //console.log(data);
          this.vendedores = data;
        },(errorService) => {
          console.log(errorService);
        });


  }

  save(forma:NgForm){
    this.pandero.postContrato(forma.value)
          .subscribe( (data:any) => {
            this.Ok = true;
          },(errorService) => {
            this.Ok = false;
          });

    forma.reset({
      asociadoID:"",
      trabajadorID:"",
      idCertificado:"",
      idProducto:"",
      vehiculo:""
    });
  }

}

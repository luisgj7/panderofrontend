import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styles: []
})
export class CardsComponent  {

  @Input() items: any[] = [];
  constructor( private router:Router) { }

  seeClient(item:any){
    let asociadoID;
    asociadoID = item.asociadoID;    
    this.router.navigate(['/client', asociadoID]);
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PanderoService } from '../../services/pandero.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {

  clients:any[] = [];
  term:string;

  constructor(private activatedRoute:ActivatedRoute,
               private pandero:PanderoService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params =>{
      this.term = params['term'];
      this.pandero.getClient( params['term']).subscribe( (data:any) => {
        this.clients = data;
        console.log(this.clients);
      });
      console.log( params['term'] ); //parametro configurado en el Router
    });

  }

}

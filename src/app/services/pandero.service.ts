import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PanderoService {

  constructor(private http: HttpClient) {
    console.log('Service Listo');
  }

  getQuery(url: string){

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });

    return this.http.get(url, {headers});
  }

  getAllClients(){
    return this.getQuery('http://localhost:8090/users/asociados/list');
  }

  getClient(term: string){
    return this.getQuery(`http://localhost:8090/users/asociados/list/${term}`);
  }

  getClientById(id: string){
    return this.getQuery(`http://localhost:8090/users/asociados/get/${id}`);
  }

  getAllVendedores(){
    return this.getQuery('http://localhost:8090/users/vendedores/list');
  }

  getAllCertificados(){
    return this.getQuery('http://localhost:8090/certificado/list');
  }

  getAllProductos(){
    return this.getQuery('http://localhost:8090/producto/list');
  }

  postContrato(body:any){

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    //console.log(JSON.stringify(body));
    return this.http.post('http://localhost:8090/ventas/contrato', body, {headers});
  }

}
